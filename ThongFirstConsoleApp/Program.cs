﻿using Newtonsoft.Json;
using System;

namespace ThongFirstConsoleApp
{

    class Student
    {
        public int Age { get; set; }
        public string Name { get; set; }
        public string ID { get; set; }
    }
    class Program
    {
        static void Main(string[] args)
        {
            Student Thong = new Student();

            Thong.Age = 22;
            Thong.Name = "Hồ Văn Thông";
            Thong.ID = "16521533";

            string jsonString = JsonConvert.SerializeObject(Thong);

            Console.WriteLine(jsonString);
        }
    }
}
